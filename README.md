# oSchoolZone
# Creating new App on frontend:
1. oschoolzone>ng new school --skipInstall=true --minimal=true --createApplication=false --skipGit=true --style=scss --skipTests=true       --prefix=sz --packageManager=yarn --newProjectRoot=frontend --directory=./
2. oschoolzone>ng g application school --minimal=true --routing --style=scss --skipTests=true --prefix=sz --skipTests=true
3. oschoolzone>ng g application admin --minimal=true --routing --style=scss --skipTests=true --prefix=sz --skipTests=true

# To Run the project: 
1. ng serve --project=admin
2. ng serve --project=school

# Creating new App on backend:
1. oschoolzone>nest new backend -s -g -p yarn
2. oschoolzone>yarn

To lint the project use: oschoolzone>ng lint

To run frontend and backend project at once need to add dependency: yarn add -D concurrently
Set: "start": "concurrently -c \"green.bold, blue.bold\" -n \"API,SCHOOL\" \"yarn start:server\" \"yarn start:client
    --progress=false --verbose=false\""

# To run both the applciation at once: >yarn start

# Mongo DB configuration: 
1. Add the configuration parameter to .env file and add using >yarn add dotenv command.

# Adding Angular material to the project:
1. yarn add @angular/material
2. Add material module to shared project: oschoolzone>ng g m material --project=shared
3. Adding material to sandbox project: oschoolzone>ng add @angular/material --project=sandbox
4. Running the sandbox project: oschoolzone>ng serve --project=sandbox

# Modularization of backend:
1. To create new backend module: oschoolzone>nest g module controllers/user --no-spec
2. To create new contoller: oschoolzone>nest g controller controllers/user --no-spec
3. To generate interceptor: oschoolzone>nest g interceptor handlers/interceptors/global --no-spec