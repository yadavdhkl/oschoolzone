import { NestFactory } from '@nestjs/core';
import { GlobalInterceptor } from 'backend/handlers/interceptors/global.interceptor';
import { AppModule } from './app.module';
import { config } from './config';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  const { appName, port, version } = config;
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix(version);
  app.useGlobalInterceptors(new GlobalInterceptor());

  const options = new DocumentBuilder()
    .addServer("/${version}")
    .setTitle('AppName')
    .setDescription('The API Documentation')
    .setVersion(version)
    .addTag('cats')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  await app.listen(port, () => {
    console.info(`${appName} is running at http://localhost:${port}/${version}`);
  });
}
bootstrap();

