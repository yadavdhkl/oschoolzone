import * as dotenv from 'dotenv';

const result = dotenv.config();

if (result?.error) {
    throw new Error('Add .env file');
}

export const config = {
    env: process.env.SZ_ENV,
    appName: process.env.SZ_APP,
    port: process.env.SZ_PORT,
    version: process.env.SZ_VERSION,
    mongoUri: `mongodb+srv://${process.env.SZ_MONGO_USER}:${process.env.SZ_MONGO_PASS}@${process.env.SZ_MONGO_HOST}.${process.env.SZ_MONGO_REPLICA}/${process.env.SZ_MONGO_DB}?authSource=admin&replicaSet=atlas-h90k52-shard-0&readPreference=primary&ssl=true`
    // mongoUri: 'mongodb+srv://service:service123@oschoolzone.dg3bd.mongodb.net/test?authSource=admin&replicaSet=atlas-h90k52-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true'
};