import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { config } from './config';
import { UserModule } from 'backend/controllers/user/user.module';
@Module({
  imports: [MongooseModule.forRoot(config.mongoUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
  }), UserModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
