
import { Common, Permissions } from "@sz/interface";
import { Role } from "backend/enums";

export interface User extends Common {
    userName: string;
    firstName?: string;
    lastName?: string;
    displayName?: string;
    role: Role;
    permissions?: Permissions;
    passwordHash?: string;
    password?: string;
    confirmPassword?: string;
}

export type Login = Required<Pick<User, 'userName' | 'password'>>;
export type Register = Required<Pick<User, 'userName' | 'password' | 'confirmPassword' | 'role'>> & Common;
