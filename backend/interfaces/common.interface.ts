import { Actions } from "backend/enums";
import { Document } from "mongoose";

export interface AppStatus {
    message: string;
}
export interface Permissions {
    [index: string]: Actions[];
}
export interface Common extends Document {
    createdBy?: string;
    updatedBy?: string;
    voidedBy?: string;
    void?: boolean;
}
